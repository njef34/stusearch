This is the README for the stusearchv3 python program
This program and subsequent README were written by
  NICHOLAS J EGGLESTON
  Alpha Class, no. 25

There are some optional dependencies to this program, but if I've done everything right you should
just be able to run this like a normal Windows program (through the command line of course)

Optional Dependencies:
Python 2.7 (Python 3.x will not work)
Python Mechanize library

You'll have several files in the stusearchv3 directory. There is a stusearchv3.exe executable that will run on Windows.

I haven't tested this compiled version on Linux so if you run Linux (and assumably know what you're doing) then
you can just run the stusearchv3.py file with the python interpreter, passing the path to the names.txt as a command line argument.

First, you must have a "names.txt" file (not a .doc or anything like that, just a plain text file)
containing the full name of who you're searching for. It used to have to be named names.txt, but I've
since removed that requirement so you can in theory call it anything you like, names.txt just makes sense.

It's best to structure this

Lastname, Firstname Middlename (Eggleston, Nicholas J)

but it should work as Firstname Lastname or Firstname Middlename Lastname as well
Each name should be on its own separate line in the names.txt file.

If you run Linux, I expect you know how to open Terminal. If you're running this in Windows, the first step
to actually running the program is to open the Command Prompt. Hold down the Windows key and then hit the R key.
This should pop up a little run dialog box. In the box type "cmd" and hit enter. You should then be in Command Prompt.
In all likelyhood you'll start out in C:\User\YourName if you're in Windows 7 or above. Use the command 'cd' (which stands for Change Directory)
to go to the directory you have the stusearchv3 directory stored.

Once you have your list of names (in names.txt) and you're in the right directory in the command line/terminal for your operating system
run:

stusearchv3.exe C:\path\to\names.txt

Assuming you have a valid names.txt, you have a connection to the internet, and the school hasn't
rewritten (again) how the people.mst.edu site works the program should just run
and run
and run
and run some more.

It takes a while, each name is a separate instance of filling out the form on that website,
hitting submit, waiting for the school's servers to respond with a list of possible email addresses
parsing the email addresses, adding that to a list, and going again.

I'm guessing you'll have somewhere near 600 names or better (if you're doing it right) so that'll take
about an hour or so to complete, depending on your computer and internet speed. I haven't written in any
sort of progress bar for this program, sorry about that, hopefully that'll come in the next release.

When you're all done you'll have a file called psp.txt in the same directory as the program.
This file will have on each line the email address for the person who's name was given
to the program. If we couldn't find the person's email address, we don't guess, they just won't be included.

From there you can copy the whole long list of email addresses into the BCC line of your email
(For the love of god and all things holy don't cc everyone, that's a great way to piss people off for
a whole host of reasons) and send them out the lovely, friendly yet professional email explaining to them
why they should join PSP and why it's a fantastic organization, etc. etc.

If you have any questions, you can email me at nickegg1018@gmail.com and hopefully I'll still be with-it
enough to be able to help you debug this program.

NOTE TO NICK: Executable created with http://www.pyinstaller.org/
