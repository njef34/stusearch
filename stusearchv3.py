#!/usr/bin/env python

import re
from mechanize import Browser
import sys

'''
This program is the intellectual property of:
  NICHOLAS J EGGLESTON
This program's purpose is to search the people.mst.edu
directory website and return a list of email addresses
for the given list of full student names. For more details
visit the README

version = 3.0
last revision = 14 September 2017
'''

a = []

try:
    herp = open(sys.argv[1], 'r').read()
    stuname = herp.split('\n')
except IndexError:
    print "You have to tell me where to look for the names! \nIt should look like this > \"python stusearchv3.py C:\\this\\is\where\\the\\names\\are.txt\""
    exit(1)

for name in stuname:
  br = Browser()
  br.open("https://people.mst.edu")
  br.select_form(name="directory")
  # Browser passes through unknown attributes (including methods)
  # to the selected HTMLForm (from ClientForm).
  br["name"] = name  # (the method here is __setitem__)
  response = br.submit()  # submit current form

  for line in response:
    derp = re.search("mailto*.", line)
    if derp != None:
      beg = line.find("mailto:")
      end = line.find(".edu")
      #print "("+str(beg)+", "+str(end)+")"
      address = line[beg+7:end+4] #.split("edu")[0]+"edu" #+7 gets you past "mailto:" and +4 includes the ".edu"
      #print address
      if address != "webmaster@mst.edu" and end != -1: #The site as it stands has 3 occurances of "mailto" One is mailto:NA (end=-1) and the other is mailto:webmaster@mst.edu. We obviously don't want those
        a.append(address)

f = open('psp.txt','w')
for i in a:
  f.write(i)
  f.write('\n')
f.close()
