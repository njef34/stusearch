This is the README for the stusearchv2.py python program
This program and subsequent README were written by
  NICHOLAS J EGGLESTON
  Alpha Class, no. 25
  
This program has a few dependenceies that must be resolved before 
it can be used. You must install:

Python 2.6 or higher
Python mechanize extension

It also doesn't hurt to run Linux but is not required.
It also doesn't hurt to have a working knowledge of Python, but hopefully
this README can guide you through everything you'll need to know

First, you must have a "names.txt" file (not a .doc or anything like that, just a plain text file)
containing the full name of who you're searching for. It's best to structure this

Lastname, Firstname Middlename (Eggleston, Nicholas J)

but it should work as Firstname Lastname or Firstname Middlename Lastname as well

Each name should be on its own separate line in the names.txt file. 
Did I mention that it has to be called names.txt
The program searches for a file called that, it must be called names.txt and must be in the
same directory/folder as the python program, otherwise it won't know where to look and
nothing will happen.

Once you have your list of names (in names.txt) in the command line/terminal for your operating system
run:

python stusearchv2.py

Assuming you have a valid names.txt, you have a connection to the internet, and the school hasn't
rewritten (again) how the people.mst.edu site works (hence the v2) the program should just run
and run
and run
and run some more.

It takes a while, each name is a separate instance of filling out the form on that website,
hitting submit, waiting for the school's servers to respond with a list of possible email addresses
parsing the email addresses, adding that to a list, and going again.

I'm guessing you'll have somewhere near 600 names or better (if you're doing it right) so that'll take
about an hour or so to complete, depending on your computer and internet speed.

When you're all done you'll have a file called psp.txt in the same directory as the program and
names.txt. This file will have on each line the email address for the person who's name was given
to the program. If we couldn't find the person's email address, we don't guess, they just won't be included

From there you can copy the whole long list of email addresses into the BCC line of your email
(For the love of god and all things holy don't cc everyone, that's a great way to piss people off for
a whole host of reasons) and send them out the lovely, friendly yet professional email explaining to them
why they should join PSP and why it's a fantastic organization, etc. etc.

If you have any questions, you can email me at nickegg1018@gmail.com and hopefully I'll still be with-it
enough to be able to help you debug this program. 